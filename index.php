<html>
<head>
<title>Decide2Screen</title>

<style type="text/css">
@font-face {
	font-family: "Segoe UI Symbol"; src: url("//db.onlinewebfonts.com/t/150ed9b2a009a71d2d819b5561167302.eot"); src: url("//db.onlinewebfonts.com/t/150ed9b2a009a71d2d819b5561167302.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/150ed9b2a009a71d2d819b5561167302.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/150ed9b2a009a71d2d819b5561167302.woff") format("woff"), url("//db.onlinewebfonts.com/t/150ed9b2a009a71d2d819b5561167302.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/150ed9b2a009a71d2d819b5561167302.svg#Segoe UI Symbol") format("svg");
}

.navText{font-family:"Segoe UI Symbol", "Segoe UI";}

h1{
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size:24pt;
	line-height: 100%;
	text-align: center;
	color: #548235;
}

h2{
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size:24pt;
	line-height: 100%;
	text-align: center;
	color:#243599;
}

h3{
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size: 1.1em;
	font-weight: bold;
	margin: 10px 0 10px;
	line-height: 100%;
	color: #CC3399;
	border: none;
}

h4{
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size: larger;
}

p{
	font-family:"Segoe UI Symbol", "Segoe UI";
}

body{
	font-family:"Segoe UI Symbol", "Segoe UI";
}

.minimalText{
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size:18pt;
	line-height:150%;
	color:#243599;
}

.minimalTextAccent{
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size:18pt;
	line-height:150%;
	color:#548235;
}

.heavyText{
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size:13pt;
	line-height:150%;
	color:#243599;
}

.heavyTextAccent{
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size:13pt;
	line-height:150%;
	color:#548235;
}

.disclaimer{
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size:10pt;
	color:#243599;
	line-height:120%;
}

.bodyTextPink{
	font-family:"Segoe UI Symbol", "Segoe UI";
	color:#CC3399;
}

.bodyTextBlue{
	font-family:"Segoe UI Symbol", "Segoe UI";
	color:#00184C;
}

.bodyTextGreen{
	font-family:"Segoe UI Symbol", "Segoe UI";
	color:#009966;
}

.large {
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size:18px;
	line-height:100%;
}

.strong{
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-weight:bold;
}

ul li{
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size: 11pt;
	padding-bottom:0px;
}

table.table1{
	border-left: 1px solid #00184c;
	border-top: 1px solid #00184c;
	margin: 0;
	padding: 0;
}

table.table1 th{
	background-color: #00184c;
	color: #ffffff;
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-weight: normal;
	font-size:16px;
	padding: 7px;
	text-align:center;
}

table.table1 td{
	border-right: 1px solid #00184c;
	border-bottom: 1px solid #00184c;
	margin: 0px;
	padding: 14px;
	font-family:"Segoe UI Symbol", "Segoe UI";
	font-size:16px;
	text-align:left;
}

table.table_risk2 td.gc0, table.table_risk2 th.gc0{
	background-color: #ffffff;
	height: 3px;
	line-height: 3px;
}

table.table_risk2 td.gc1{
	background-color: #3366cc;
}

table.table_risk2 td.gc2{
	background-color: #33cc66;
}

table.table_risk2 td.gc3{
	background-color: #cc6633;
}

table.table_risk2 td.gc4{
	background-color: #cccccc;
}

table td
            {
                vertical-align: top;
                padding: 0 0 0 0;
                margin: 0;
                padding: 0;
                border: solid 0px red;
                }

table.table1
            {
                border-left: 1px solid #243599;
                border-top: 1px solid #243599;
                margin: 0;
                padding: 0;
            }

table.table1 th
                {
                    background-color: #243599;
                    color: #ffffff;
					font-family:"Segoe UI Symbol", "Segoe UI";
                    font-weight: normal;
                    padding: 7px;
                }

table.table1 td
                {
                    border-right: 1px solid #243599;
                    border-bottom: 1px solid #243599;
                    margin: 0;
                    padding: 7px;
                }

table.table2
            {
                border-left: 1px solid #243599;
                border-top: 1px solid #243599;
                margin: 0;
                padding: 0;
            }

table.table2 th
                {
                    background-color: #243599;
                    color: #ffffff;
					font-family:"Segoe UI Symbol", "Segoe UI";
                    font-weight: normal;
                    padding: 5px;
                }

table.table2 td
                {
                    border-right: 1px solid #243599;
                    border-bottom: 1px solid #243599;
                    margin: 0;
                    padding: 5px;
                    text-align: center;
                }

/*** risk data tables ***/
table.table_risk
        {
            border-collapse: separate;
            margin-left: 50px;
        }

table.table_risk td
        {
            background-color: #ffcccc;
            width: 10px;
            height: 20px;
            vertical-align: bottom;
        }

.table_risk th
        {
			font-family:"Segoe UI Symbol", "Segoe UI";
            font-weight: normal;
            font-size: smaller;
            position: relative;
            text-align: right;
            vertical-align: top;
        }

.grafic_num
        {
            position: relative;
            display: block;
            margin-top: -.8em;
            margin-bottom: .8em;
        }

.grafic_num img
            {
                border-style: none;
            }

table.table_risk2
        {
            border-collapse: separate; /*margin-left: 50px;*/
        }

.table_risk2 th
        {
			font-family:"Segoe UI Symbol", "Segoe UI";
            font-weight: normal;
            font-size: smaller;
            position: relative;
            text-align: right;
            vertical-align: top;
            width: 6px;
            height: 6px;
            line-height: 6px; /*
            padding: 0;
            margin: 0;
            border-top: 0px solid #ffffff;
            border-right: 2px solid #ffffff;
            border-bottom: 2px solid #ffffff;
            border-left: 0px solid #ffffff;
            */
        }

table.table_risk2 td
        {
            background-color: #ffcccc;
            width: 6px;
            height: 6px;
            line-height: 6px;
            vertical-align: bottom; /*
            padding: 0;
            margin: 0;
            border-top: 0px solid #ffffff;
            border-right: 2px solid #ffffff;
            border-bottom: 2px solid #ffffff;
            border-left: 0px solid #ffffff;
            */
        }

table.table_risk2 td.gc0, #wizard_wrap table.table_risk2 th.gc0
            {
                background-color: #ffffff;
                height: 3px;
                line-height: 3px;
            }

table.table_risk2 td.gc1
            {
                background-color: #3366cc;
            }

table.table_risk2 td.gc2
            {
                background-color: #33cc66;
            }

table.table_risk2 td.gc3
            {
                background-color: #cc6633;
            }

table.table_risk2 td.gc4
            {
                background-color: #cccccc;
            }

.grafic_num2
        {
            position: absolute;
            display: block; /*
            margin-top: -.8em;
            margin-bottom: .8em;
            */
            width: 50px;
            height: 5px;
            right: 0px;
            top: -.8em;
        }

.grafic_num3
        {
            position: absolute;
            display: block; /*
            margin-top: -.8em;
            margin-bottom: .8em;
            */
            width: 50px; /*height: 4px;*/
            right: 0px;
            top: -9px;
            line-height: 150%;
        }

/***table_risk3***/
table.table_risk3
        {
            border-collapse: separate;
        }

.table_risk3 th
        {
			font-family:"Segoe UI Symbol", "Segoe UI";
            font-weight: normal;
            font-size: smaller;
            position: relative;
            text-align: right;
            vertical-align: top;
            width: 6px;
            height: 6px;
            line-height: 6px;
            padding: 0;
            margin: 0;
        }

table.table_risk3 td.gc0, #wizard_wrap table.table_risk3 th.gc0
        {
            background-color: #ffffff;
            height: 3px;
            line-height: 3px;
        }

.table_risk3 td
        {
            width: 200px;
            height: 6px;
            line-height: 8px;
            padding: 0;
            margin: 0;
        }

.table_risk3 td img
            {
                padding: 0px 2px 2px 0;
                float: left;
            }

/***references***/
#div_references
        {
            height: 25em; /*400px;*/
            overflow: auto;
            border: 1px solid #cccccc;
            padding: 1px 1em 1em; /*0px 10px;*/
            margin: 1em 1px;
            font-size: 1em;
        }

#div_references ul, #div_references ol
        {
            padding-left: 50px;
        }

#div_references ul li, #div_references ol li
            {
				font-family:"Segoe UI Symbol", "Segoe UI";
                font-size: 1em;
                padding: 0 5px 10px;
            }

ol li span.def_term, span.def_term
        {
            /*font-weight: normal;*/
            text-decoration: underline;
            cursor: pointer;
            color: #4A68A5;
        }

ol li span.def_term2
        {
            /*font-weight: normal;*/
            text-decoration: underline;
            cursor: pointer;
            color: #4A68A5;
            display: inline;
            margin: 0;
            padding: 0;
			font-family:"Segoe UI Symbol", "Segoe UI";
            font-weight: normal;
            line-height: normal;
        }

/* Tooltip container */
.tooltip1 .tooltip2 .tooltip3 {
    position: relative;
    display: inline-block;
}

/* Tooltip text */
.tooltip1 .tooltiptext1 .tooltip2 .tooltiptext2 .tooltip3 .tooltiptext3 {
    visibility: hidden;
    background-color: black;
    color: #fff;
    text-align: center;
    padding: 5px 0;
    border-radius: 6px;
    top: -5px;
    left: 105%; 
	font-family:"Segoe UI Symbol", "Segoe UI";
    font-size: 40px; /* this changes your hover text font size */
}

/* Show the tooltip text when you mouse over the tooltip container */
.tooltip1:hover .tooltiptext1 {
    visibility: visible;
}

.tooltip2:hover .tooltiptext2 {
    visibility: visible;
}

.tooltip3:hover .tooltiptext3 {
    visibility: visible;
}
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script><script type="text/javascript">
<!--
    function backButtonOverride() { setTimeout("backButtonOverrideBody()", 1); } // Work around a Safari bug that sometimes produces a blank page
    function backButtonOverrideBody() {
        // Works if we backed up to get here
        try { history.forward(); } catch (e) { } // OK to ignore 
        // Every quarter-second, try again. The only guaranteed method for Opera, Firefox,  and Safari, 
        // which don't always call  onLoad but *do* resume any timers when returning to a page
        setTimeout("backButtonOverrideBody()", 125);
    }
    document.oncontextmenu = function () { return false } //stops the browser context menu from appearing on right click

    function ShowInfo(info) {
        var winW = 630, winH = 460;
        if (document.body && document.body.clientWidth) {
            winW = document.body.clientWidth;
            winH = document.body.clientHeight;
        }
        if (document.compatMode == 'CSS1Compat' && document.documentElement && document.documentElement.offsetWidth) {
            winW = document.documentElement.offsetWidth;
            winH = document.documentElement.offsetHeight;
        }
        if (window.innerWidth && window.innerHeight) {
            winW = window.innerWidth;
            winH = window.innerHeight;
        }
        var liquid = document.getElementById('liquid-round');
        var dialog = document.getElementById('dialog_id');
        var dialog_title = document.getElementById('dialog_title');
        var dialog_content = document.getElementById('dialog_content');
        var title, content;

        switch (info) {
            case 'invasive breast cancer':
                title = 'invasive breast cancer';
                content = 'Cancer that has spread from where it started in the breast into surrounding, healthy tissue. Most invasive breast cancers start in the ducts (tubes that carry milk from the lobules to the nipple). Invasive breast cancer can spread to other parts of the body through the blood and lymph systems. Also called infiltrating breast cancer.';
                break;
            case 'ductal carcinoma in situ':
                title = 'ductal carcinoma in situ (DCIS)';
                content = 'A noninvasive condition in which abnormal cells are found in the lining of a breast duct. The abnormal cells have not spread outside the duct to other tissues in the breast. In some cases, ductal carcinoma in situ may become invasive cancer and spread to other tissues, although it is not known at this time how to predict which lesions will become invasive. Also called intraductal carcinoma.';
                break;
            case 'lobular carcinoma in situ':
                title = 'lobular carcinoma in situ (LCIS)';
                content = 'A condition in which abnormal cells are found in the lobules of the breast. LCIS seldom becomes invasive cancer; however, having lobular carcinoma in situ in one breast increases the risk of developing breast cancer in either breast.';
                break;
            case 'menstrual period':
                title = 'menstrual period';
                content = 'The periodic discharge of blood and tissue from the uterus. From puberty until menopause, menstruation occurs about every 28 days, but does not occur during pregnancy.';
                break;
            case 'biopsy':
                title = 'biopsy';
                content = 'The removal of cells or tissues for examination by a pathologist. The pathologist may study the tissue under a microscope or perform other tests on the cells or tissue. When only a sample of tissue is removed, the procedure is called an incisional biopsy. When an entire lump or suspicious area is removed, the procedure is called an excisional biopsy. When a sample of tissue or fluid is removed with a needle, the procedure is called a needle biopsy, core biopsy, or fine-needle aspiration.';
                break;
            case 'atypical hyperplasia':
                title = 'atypical hyperplasia';
                content = 'A benign (noncancerous) condition in which cells look abnormal under a microscope and are increased in number.';
                break;
            case 'race_hispanic':
                title = '';
                content = 'Assessments for Hispanic women are subject to greater uncertainty than those for white and African American women. Researchers are conducting additional studies, including studies with minority populations, to gather more data and to increase the accuracy of the tool for women in these populations.';
                break;
            case 'race_amrican_indian':
                title = '';
                content = 'Assessments for American Indian or Alaskan Native women are uncertain and are based on data for white women. Researchers are conducting additional studies, including studies with minority populations, to gather more data and to increase the accuracy of the tool for women in these populations.';
                break;
            case 'race_unknown':
                title = '';
                content = 'This risk assessment was based on data for white females.';
                break;
            case 'false positive test':
                title = 'false positive test ';
                content = 'A false positive test is an abnormal finding that turns out not to be cancer.';
        }

        dialog_title.innerHTML = title;
        dialog_content.innerHTML = content;

        dialog.style.display = 'block';
        dialog.style.position = 'fixed';

        var left = eval((winW - dialog.offsetWidth) / 2);
        var top = eval((winH - dialog.offsetHeight) / 2 - 30);

        if (top < 0) {
            top = 0;
            dialog.style.position = 'absolute';
        } else
            if (left < 0) {
                left = 25;
                dialog.style.position = 'absolute';
            }

        dialog.style.left = left + 'px';
        dialog.style.top = top + 'px';
        dialog.style.zIndex = 1000;

    }
    function HideInfo() {
        var dialog = document.getElementById('dialog_id');
        dialog.style.display = 'none';
        return false;
    }

    /*** ATTACH DON'T KNOW CHECK ***/

    //*** This code is copyright 2003 by Gavin Kistner, gavin@refinery.com
    //*** It is covered under the license viewable at http://phrogz.net/JS/_ReuseLicense.txt
    //*** Reuse or modification is free provided you abide by the terms of that license.
    //*** (Including the first two lines above in your source code satisfies the conditions.)
    //***Cross browser attach event function. For 'evt' pass a string value with the leading "on" omitted
    //***e.g. AttachEvent(window,'load',MyFunctionNameWithoutParenthesis,false);

    function AttachEvent(obj, evt, fnc, useCapture) {
        if (!useCapture) useCapture = false;
        if (obj.addEventListener) {
            obj.addEventListener(evt, fnc, useCapture);
            return true;
        } else if (obj.attachEvent) return obj.attachEvent("on" + evt, fnc);
        else {
            MyAttachEvent(obj, evt, fnc);
            obj['on' + evt] = function () { MyFireEvent(obj, evt) };
        }
    }

    //The following are for browsers like NS4 or IE5Mac which don't support either attachEvent or addEventListener
    function MyAttachEvent(obj, evt, fnc) {
        if (!obj.myEvents) obj.myEvents = {};
        if (!obj.myEvents[evt]) obj.myEvents[evt] = [];
        var evts = obj.myEvents[evt];
        evts[evts.length] = fnc;
    }
    function MyFireEvent(obj, evt) {
        if (!obj || !obj.myEvents || !obj.myEvents[evt]) return;
        var evts = obj.myEvents[evt];
        for (var i = 0, len = evts.length; i < len; i++) evts[i]();
    }
    //R: don't know 
    function Check_DK(el, li_id) {
        if (typeof (event) != 'undefined') el = event.srcElement;    // IE<9 fix
        var li = document.getElementById(li_id);
        var arr_input = li.getElementsByTagName('input');
        if (arr_input.length > 0) {
            if (el.id == arr_input[arr_input.length - 1].id) {
                if (el.checked) for (var i = 0; i < arr_input.length - 1; ++i) arr_input[i].checked = false;
            } else {
                if (el.checked) arr_input[arr_input.length - 1].checked = false;
            }
        }
    }
    function Attach_LiDK(li_id) {
        var li = document.getElementById(li_id);
        var arr_input = li.getElementsByTagName('input');
        if (arr_input.length > 0) {
            for (var i = 0; i < arr_input.length; ++i) {
                AttachEvent(arr_input[i], 'click', function () { Check_DK(this, li_id) }, false);
            }
        }
    }
/*************************************/
//--> 
</script><!-- dialog -->
</head>
<body>
<div class="dialog" id="dialog_id" style="width: 500px; display: none; position: fixed;"><!-- top/right PNG -->
<div class="content" style="z-index: 2;">
<div style="float: right; margin-right: -12px; margin-top: -12px;"><input alt="close window" id="ButtonClose" onclick="return HideInfo();" src="https://decide2screen.org/images/dialog/close.png" style="border-width: 0px;" type="image" value="button" /></div>
<!-- optional: vertical scroll wrapper -->

<div class="wrapper"><!-- top/left PNG -->
<div class="t">&nbsp;</div>
<!-- Your content goes here --><!-- Optional: Standard Module Format (hd/bd/ft) for providing semantic, meaningful content. Not required for visual effect. -->

<div class="hd">
<h2 id="dialog_title">&nbsp;</h2>
</div>

<div class="bd">
<p id="dialog_content" style="border-top: 1px solid #cccccc;">&nbsp;</p>
</div>

<div class="ft">&nbsp;</div>
<!-- end content --></div>
<!-- end scroll wrapper -->

<div style="text-align: center; position: absolute; width: 100%; z-index: 2; bottom: -30px; left: -12px;"><a href="javascript: ;" id="ButtonCancelDialog" onclick="return HideInfo();" style="position: relative; z-index: 2;">close window</a></div>
</div>
<!-- bottom PNG -->

<div class="b">
<div>&nbsp;</div>
</div>
<!-- end dialog --></div>
<!-- end dialog -->

<!--BEGIN HEADER-->
<table border="0" cellpadding="0" cellspacing="0" style="background-color:#243599;border-left:0px solid #243599;border-right:0px solid #243599;" width="100%">
	<tbody>
		<tr>
			<td align="left" valign="middle"><img src="https://upenn.co1.qualtrics.com/ControlPanel/Graphic.php?IM=IM_6zXLmoOT3ENbQGi" style="margin-top:4px;margin-bottom:0px;margin-left:4px;" width="139" /></td>
			<td align="center" style="padding-top:40px;padding-left:0px;padding-right:0px;"><span style="font-family:'Segoe UI Symbol', 'Segoe UI';font-size:18pt;color:#ffffff;">Making Decisions: When should I have mammograms?</span></td>
		</tr>
	</tbody>
</table>
<!--END HEADER-->

<!--DYNSMIC CONTENT AREA-->

<!--BEGIN FOOTER-->
<table cellpadding="0" cellspacing="0" height="100" style="background-color:#243599;" width="100%">
	<tbody>
		<tr>
			<td align="center" valign="middle" width="50%"><span style="text-align:center;"><img alt="University of Colorado Anschutz Medical Campus" height="45" src="https://upenn.co1.qualtrics.com/ControlPanel/Graphic.php?IM=IM_9ypQc3lQkpHltQi" style="vertical-align:middle;margin-top:25px;" width="225" /></span></td>
			<td align="center" valign="middle" width="50%"><img alt="University of Pennsylvania" height="45" src="https://upenn.co1.qualtrics.com/ControlPanel/Graphic.php?IM=IM_77M4EsFaHALdFDE" style="vertical-align:middle;margin-top:25px;" width="225" /></td>
		</tr>
	</tbody>
</table>
<!--END FOOTER-->
</body>
</html>