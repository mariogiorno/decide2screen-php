**Decide2Screen-PHP**

This project is a re-development or an original version created in 2014-2015 using ASP.Net 4.0 and C#. This version converts the original to a LAMP environment to make it easier to update and secure.

The goals of this project are:

1. Create an updated version of the HCI breast cancer decision making tool that uses the Gail model to create a statistical risk score.
2. Update the server environment (currently Windows 2013 running IIS 7 and ASP.Net framework 4.0) which is old and running to its end of life.
3. Replace the older Gail calculation DLL binary with a more current version from the [DCEG website](https://dceg.cancer.gov/tools/risk-assessment) that are available in:
     A. an R package
	 B. a SAS macro
4. Redesign the website user interface.
5. Maintain the same secure server environment required of the original project that adheres to HIPAA security standards for patient health information (PHO).
